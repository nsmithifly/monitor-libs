<?php

class FlightSessions {

    function __construct($subdomain, $apikey, $transport_tls = true) {
        $this->subdomain = $subdomain;
        $this->apikey = $apikey;
        $this->transport = $transport_tls ? 'https' : 'http';
		$this->cache = new GhettoCache();
    }
    
    function ApifyHelmet($manifest) {
        $apify = array();
        // $output[$data->date][$data->reserv_no][$data->mastertran][$is_parent][] = $data;
        foreach ($manifest as $m => $time_slot) {
            $minutes_used = 0;
            foreach ($time_slot as $reserve_no => $mtrans) {
                foreach ($mtrans as $mastertran => $flight_set) {
                    foreach ($flight_set as $parent => $flight_slots) {
                        foreach ($flight_slots as $f => $flight) {
                            if (is_object($flight))
                                $minutes_used += round($flight->minutes * 4, 0)/4;
                        }
                    }
                }
            }
            $apify[] = array(
                'ecomm_available' => -1,
                'session_available' => -1,
                'session_time' => date('H:i', strtotime($m)),
                'session_label' => date('h:ia', strtotime($m)),
                'price_type' => 'price_1',
                'qty_remaining' => max(0, 30 - $minutes_used),
                'day_part' => ''
            );
        }
        return $apify;
    }
    
    function GetHelmetManifestSessions($tunnel, $date) {

        $data = array();
        $data['start'] = date("Y-m-d", strtotime($date));
        $data['end'] = date('Y-m-d', strtotime($date . ' + 2 days'));
        $post_vars = array(
            'apikey' => $this->apikey,
            'controller' => 'siriusware',
            'method' => 'get_unified_manifest',
            'language' => 'english',
            'start' => date("Y-m-d", strtotime($date)),
            'end' => date('Y-m-d', strtotime($date . ' + 2 days'))
        );
        $url = "{$this->transport}://{$this->subdomain}.iflyworld.com/siriusware/get_unified_manifest/format/json";
        $manifest = json_decode(json_encode($this->Api2RestPost($url, $post_vars)));

        $output = array();
        $data = array();
        $post_vars['controller'] = 'sessiondata';
        $post_vars['method'] = 'get_unified_manifest';
        $url = "{$this->transport}://{$this->subdomain}.iflyworld.com/sessiondata/get_helmets/format/json";
        $helmets = json_decode(json_encode($this->Api2RestPost($url, $post_vars)));

        if (isset($manifest->data) && $manifest->status == "OK") {

            // init blank times:
            $output = array();


            foreach ($manifest->data as $mk => $mv) {

                if (strtotime($mv->date) >= strtotime($date . " 06:00:00") && strtotime($mv->date) < strtotime($date . " 06:00:00 + 1 day")) {

                    $data = $mv;
                    $data->helmet_no = "";
                    $data->status = "valid";
                    $data->minutes = round($data->minutes, 1);
                    $data->session_time = date("g:i a", strtotime($data->date));
                    $data->booking_date = date("m/d/Y h:i a", strtotime($data->booking_date));

                    if (isset($data->guest_no)) {
                        foreach ($helmets->data as $hk => $hv) {
                            //$hv->data = "27121000000";
                            if ($hv->guest_no == $data->guest_no) {
                                $data->helmet_no = $hv->data;
                                break;
                            }
                        }
                    }

                    if ($data->mastertran == $data->trans_no)
                        $is_parent = "A";
                    else
                        $is_parent = "B";

                    $output[$data->date][$data->reserv_no][$data->mastertran][$is_parent][] = $data;
                }
            }
        }

        // fill in the blanks
        for ($i = 0; $i <= 23; $i++) {

            if ($i < 10)
                $hour = "0" . $i;
            else
                $hour = $i;
           
            $time00 = date('Y-m-d', strtotime($date)) . " " . $hour . ":00:00.000";
            $time30 = date('Y-m-d', strtotime($date)) . " " . $hour . ":30:00.000";

            if (!isset($output[$time00]))
                $output[$time00]["null"]["null"]["null"][] = array("status" => "empty", "session_time" => date("g:i a", strtotime($date . " " . $hour . ":00:00.000")));
            if (!isset($output[$time30]))
                $output[$time30]["null"]["null"]["null"][] = array("status" => "empty", "session_time" => date("g:i a", strtotime($date . " " . $hour . ":30:00.000")));
        }

        // sort key by date after adding the extra times
        ksort($output);

        // sort by the "A", "B" keys in the is_parent field to put the parent record first
        foreach ($output as $k => $v) {
            foreach ($v as $k2 => $v2) {
                foreach ($v2 as $k3 => $v3) {
                    ksort($output[$k][$k2][$k3]);
                }
            }
        }

       return $output;
    }
	
    function getTunnels() {
        if (($tunnels = $this->cache->get("tunnels", 12 * 60 * 60)) !== false) {
            echo "return cache\n";
            return $tunnels;
        }

        echo "fetch new tunnel set\n";
        $post_vars = array(
            'apikey' => $this->apikey,
            'controller' => 'tunnel',
            'method' => 'get_tunnels',
            'language' => 'english'
        );

        $url = "{$this->transport}://{$this->subdomain}.iflyworld.com/tunnel/get_tunnels/format/json";

        $tunnels = $this->Api2RestPost($url, $post_vars);

        return $this->cache->set("tunnels", $tunnels);
    }
    
    function iflyWorldApi($tunnel, $controller, $method, $data) {

        $data["tunnel"] = $tunnel;
        $apikey = "c6af7b9ef5c13869332319b7d9087fe5";
        $format = "json";

        $data['controller'] = $controller;
        $data['method'] = $method;
        $data['apikey'] = $apikey;
        $data['format'] = $format;


        //print_r($data);
        // build cURL's URL
        $url = "http://api.iflyworld.com/index.php/$controller/$method/apikey/$apikey/format/$format";

        if ( $tunnel == "PEN" ||  $tunnel == "GC"){
            $url = "http://aws-api.iflyworld.com/index.php/$controller/$method/apikey/$apikey/format/$format";
        }

        echo $url . "\n";
        print_r($data);
		// initialize cURL
        if (($ch = @curl_init()) == false) {
            throw new Exception("Cannot initialize cURL session");
        }

        // set cURL options
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        // DNS Cache timeout
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);

        // Ignore passthru signals
        curl_setopt($ch, CURLOPT_NOSIGNAL, TRUE);

        // Set total session timeout
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);

        // Set max socket connect timeout
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        // execute cURL
        $result = curl_exec($ch);

    // Check for cURL errors
        if (curl_error($ch)) {
            throw new \RuntimeException("Communication with the API [" . $url . "] failed: " . curl_error($ch));
        }

        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($http_status !== 200) {
            throw new \Exception("Something went wrong: " . $http_status . " - " . print_r($result, true));
        }

        //echo $result;
        // validate if it is a json string
        $data = @json_decode($result); // supress warning using @ operator
        if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
            $json_error['status'] = "JSON_ERR";
            $json_error['json'] = $result;
            throw new \Exception('Invalid server output: ' . print_r($result, 1));
        }

    // Formally close socket
        curl_close($ch);

        return $result;
    }
    
    function GetApi2Sessions($tunnel, $dci, $date) {
        $post_vars = array(
            'apikey' => $this->apikey,
            'tunnel' => $tunnel,
            'controller' => 'siriusware',
            'method' => 'get_sessions',
            'language' => 'english',
            'padded_DCI' => $this->FixDci($dci),
            'date' => date("Y-m-d", strtotime($date))
        );

        $url = "{$this->transport}://{$this->subdomain}.iflyworld.com/siriusware/get_sessions/format/json";

        return $this->Api2RestPost($url, $post_vars)['data'];
    }
        
    function FixDCI($dci) {
        $dci_tmp = array_map(function ($d) { return trim($d); }, preg_split('/ /', $dci, -1, PREG_SPLIT_NO_EMPTY));
        if (count($dci_tmp) == 3)
            return implode('', array_map(function($d) { return str_pad($d,10,' '); }, $dci_tmp));
        throw new \Exception('Invalid Padded DCI: $dci');
    }
    
    function Api2RestPost($endpoint, $params) {

        //$data['XDEBUG_SESSION_START']= 'netbeans-xdebug';
        // initialize cURL
	//echo "init curl\n";
        if (($ch = @curl_init()) == false) {
            throw new Exception("Cannot initialize cURL session");
        }

	//echo "set curl options\n";
        // set cURL options
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_NOSIGNAL, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . md5(microtime()) . '; path=/');

        // execute cURL
	//echo "curl_exec()\n";
        $result = curl_exec($ch);

        if (curl_error($ch)) {
            return("Communication with the API [" . $url . "] failed: " . curl_error($ch));
        }

        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        curl_close($ch);

        if ($http_status !== 200) {
            return("Something went wrong: " . $http_status . ": " . print_r($result, true));
        }

        // validate if it is a json string

        $test_decode = @json_decode($result, true); // suppress warning using @ operator
        
	//print_r($test_decode);

        if ($test_decode === NULL && json_last_error() !== JSON_ERROR_NONE) {
            $json_error['status'] = "JSON_ERR";
            $json_error['json'] = $result;
            throw new \Exception('Invalid server output: ' . print_r($result, 1));
        }
        
        return $test_decode;

    }

}

