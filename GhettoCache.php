<?php

class GhettoCache {
		
	function get($action, $lifetime) {
		$stat = @stat("/var/tmp/iflyworld.$action.cache");
		if ($stat === false)
			return false;
		if ($stat['mtime'] < time() - $lifetime)
			return false;
		
		return json_decode(file_get_contents("/var/tmp/iflyworld.$action.cache"), true);
	}
	
	function set($action, $content) {
		$cache = $content;
		if (! is_string($cache)) {
			$cache = json_encode($cache, !JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
			if (json_last_error() != JSON_ERROR_NONE)
				return;
		}
		file_put_contents("/var/tmp/iflyworld.$action.cache", $cache, LOCK_EX);
		return $content;
	}
}