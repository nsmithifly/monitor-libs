<?php

class SystemMonitor {
    
    var $gelf;
    var $profiler;
    var $_other_fields;
    var $_send_msg;
    var $_timers;
    var $_timer_methods;
    
    function __construct($class = null, $application = null) {
        $this->gelf = new stdClass();
        $this->gelf->endpoint = '10.0.100.188';
        $this->gelf->port = 12202;
        $this->gelf->sock_is_dirty = false;
        if (($this->gelf->sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) !== false) {
            $this->gelf->sock_is_dirty = true;
        }
        
        $this->_timers = array();
        $this->_timer_methods = array();
        
        $this->profiler = new stdClass();
        $this->profiler->endpoint = '10.0.100.187';
        $this->profiler->port = 8125;
        $this->profiler->sock_is_dirty = false;
        $this->profiler->class = $class;
        $this->profiler->application = $application;
        $this->profiler->host = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : (isset($_SERVER['LOCAL_ADDR']) ? $_SERVER['LOCAL_ADDR'] : gethostname());
        if (($this->profiler->sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) !== false) {
            $this->profiler->sock_is_dirty = true;
        }
        $this->_other_fields = array();
    }
    
    function __set($field, $value) {
        $this->_other_fields[$field] = $value;
    }
    
    function triggerPagerDuty($service_key, $description, $url, $details, $incident_key = null, $contexts = null) {
        $endpoint = "https://events.pagerduty.com/generic/2010-04-15/create_event.json";
       
        $msg = array(
                'service_key' => $service_key,
                'event_type' => 'trigger',
                'description' => $description,
                'details' => $details,
                'client' => $this->profiler->application,
                'client_url' => $url
            );
        
        if (isset($incident_key))
            $msg['incident_key'] = $incident_key;
        
        if (isset($contexts))
            $msg['contexts'] = $contexts;
        
        
        if (($ch = @curl_init()) == false) {
            throw new Exception("Cannot initialize cURL session");
        }
        
        $body = json_encode($msg);
        
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // DNS Cache timeout
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 3);

        // Ignore passthru signals
        curl_setopt($ch, CURLOPT_NOSIGNAL, TRUE);

        // Set max socket connect timeout
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        // execute cURL
        $result = curl_exec($ch);

        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        if ($http_status !== 200) {
            echo $body;
            throw new \Exception("Something went wrong: " . $http_status . ": " . curl_error($ch));
        }

        curl_close($ch);
    }
    
    function increment($counter, $method, $class = null, $application = null) {
        return $this->count($counter, 1, $class, $application, $host);
    }
    
    function count($counter, $increment, $method, $class = null, $application = null) {
        return $this->profileMsg($counter, $increment, "c", $method, $class, $application, $host);
    }
    
    function gauge($gauge, $measure, $method, $class = null, $application = null) {
        return $this->profileMsg($gauge, $measure, "g", $method, $class, $application, $host);
    }
    
    function startTimer($timer, $method, $class = null, $application = null) {
        $this->_timers[$timer] = microtime(true);
        $this->_timer_methods[$timer] = $method;
    }
    
    function stopTimer($timer) {
        $time = ceil(1000 * (microtime(true) - $this->_timers[$timer]));
        $method = $this->_timer_methods[$timer];
        unset($this->_timers[$timer]);
        unset($this->_timer_methods[$timer]);
        return $this->profileMsg($timer, $time, "ms", $method);
    }
    
    function profileMsg($key, $value, $type, $method, $class = null, $application = null) {
        $pkey = "{$this->profiler->application}.{$this->profiler->class}.$method.$key";
        $msg = sprintf("%s:%s|%s", $pkey, $value, $type);
        $len = strlen($msg);
        socket_sendto($this->profiler->sock, $msg, $len, 0, $this->profiler->endpoint, $this->profiler->port);
        
        $tunnel = isset($_POST['tunnel']) ? $_POST['tunnel'] : 'NOTUN';
        $pkey = "{$this->profiler->application}.$tunnel.{$this->profiler->class}.$method.$key";
        $msg = sprintf("%s:%s|%s", $pkey, $value, $type);
        $len = strlen($msg);
        socket_sendto($this->profiler->sock, $msg, $len, 0, $this->profiler->endpoint, $this->profiler->port);
    }
    
    function log($host, $tag, $content = false, $with_session_id = true, $with_post = false, $with_timings = true) {
        global $tracking_guid;
        $client_ip = isset($_POST['client_ip']) ? $_POST['client_ip'] : '0.0.0.0';
        $src_client_ip = (strpos($client_ip, ',') === false) ? $client_ip : explode(',',$client_ip)[0];
        $msg = array(
            "version" => 1.1,
            "host" => getHostByName(getHostName()) . "/$host",
            "short_message" => $tag,
            "level" => 1,
            "full_message" => $content,
            "_tracking_guid" => $tracking_guid,
            "_client_ip" => $client_ip,
            "_source_client_ip" => $src_client_ip,
            "_ip_address" => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'],
            "_severity" => 0
        );
        $msg["_wwsale_id"] = $_SESSION['wwsale_id'];
        if ($with_session_id)
            $msg["_session_id"] = session_id();
        if ($with_post)
            $msg["_post"] = $_POST;
        if ($with_timings)
            $msg["_timing"] = microtime(true);
        
        foreach ($this->_other_fields as $field => $value) {
            $msg["_" . $field] = $value;
        }
        
        $this->_send_msg = gzcompress(json_encode($msg), 9);
        $this->clean();
        $len = strlen($this->_send_msg);
        
        if ($len > 8192) {
//            die(print_r($msg, true));
            return;
        }
        
        return socket_sendto($this->gelf->sock, $this->_send_msg, $len, 0, $this->gelf->endpoint, $this->gelf->port);
    }
    
    function clean() {
        $this->_other_fields = array();
    }
    
    function close() {
        if ($this->gelf->sock_is_dirty)
            socket_close($this->gelf->sock);
    }
    
}
