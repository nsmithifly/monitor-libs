<?php

class VarLog {
	function __construct($application, $max_days) {
		$this->logpath = "/var/log/monitoring/$application/$application.log";
		if (!file_exists("/var/log/monitoring/")) {
			mkdir("/var/log/monitoring", 0555);
		}
		if (!file_exists("/var/log/monitoring/$application")) {
			mkdir("/var/log/monitoring/$application", 0555);
		}
		if (file_exists($this->logpath)) {
			$mdate = date("Ymd", filemtime($this->logpath));
			$date  = date("Ymd");
			if ($mdate < $date) {
				rename($this->logpath, $this->logpath . ".$date");
			}
			$rotate = date("Ymd", strtotime("-$max_days days"));
			if (file_exists($this->logpath . ".$rotate")) {
				unlink($this->logpath . ".$rotate");
			}
		}
	}
	
	function log($content) {
		file_put_contents($this->logpath, "\n============== " . date("Y-m-d H:i:s") . " ============== \n", FILE_APPEND);
		file_put_contents($this->logpath, $content, FILE_APPEND);
	}
}